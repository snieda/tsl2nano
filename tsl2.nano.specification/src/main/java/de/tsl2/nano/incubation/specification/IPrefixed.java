package de.tsl2.nano.incubation.specification;

public interface IPrefixed {
	String prefix();
}
